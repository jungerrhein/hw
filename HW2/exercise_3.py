def swapper(input_str: str) -> str:

    """
        Swap 'reasonable' on 'unreasonable'

        Input:
            input_str: str
                some_str

        Output:
            answer: str
                String with swapped substrings
    """


    assert isinstance(input_str, str), \
            f"Wrong type of input data! Need str, got: {type(input_str)}"

    splitted_str = input_str.split()


    def checker(l):

        if l == "reasonable":
            return "unreasonable"
        return l

    ans = " ".join(list(map(checker, splitted_str)))

    return ans


def tests():

    """
        Base tests for program
    """
    
    input_str = "reasonable"
    
    # Basic test
    ans = swapper(input_str)

    assert ans  == "unreasonable"

    input_str = "reasonable reasonable"
    ans = swapper(input_str)

    assert ans == "unreasonable unreasonable"


def main():

    """
        Main entry point
    """

    tests()

if __name__ == "__main__":
    main()
