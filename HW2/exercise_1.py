def count_negatives(input_number: list) -> int:

    """
        Count negative values in list

        Input:
            input_number: list
                list, that contain some numbers

        Output:
            count_neg: int
                Number of negative numbers
    """


    # Lazy mode, use assert instead throw error

    assert isinstance(input_number, list), \
            f"Wrong type of input data! Need list, got: {type(input_number)}"

    

    return len(list(filter(lambda l: l < 0, input_number)))

def tests():

    """
        Base tests for program
    """


    # Basic test
    count_neg = count_negatives([4, -9, 8, -11, 8])
    assert count_neg == 2

    # Empty input
    count_neg = count_negatives([])
    assert count_neg == 0

    # All negatives
    count_neg = count_negatives([-1, -2, -3])
    assert count_neg == 3

    # All positives
    count_neg = count_negatives([1, 2, 3])
    assert count_neg == 0


def main():

    """
        Main entry point
    """

    tests()

if __name__ == "__main__":
    main()
