def change_positions(players: list) -> list:

    """
        Reverse list

        Input:
            players: list
                list, that contain some strings

        Output:
            reversed_players: list
                reversed list of players
    """


    assert isinstance(players, list), \
            f"Wrong type of input data! Need list, got: {type(input_number)}"

    return [players[-1]] + players[1:-1] + [players[0]]


def tests():

    """
        Base tests for program
    """
    
    players = ["Ashleigh Barty",
        "Simona Halep", "Naomi Osaka", "Karolina Pliskova",
        "Elina Svitolina"]
    # Basic test
    reversed_list = change_positions(players)
    assert reversed_list == [players[-1]] + players[1:-1] + [players[0]]


def main():

    """
        Main entry point
    """

    tests()

if __name__ == "__main__":
    main()
