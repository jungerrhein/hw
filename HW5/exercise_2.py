class Node:

    def __init__(self, val=None, pointer=None):
        self._val = val
        self._pointer = pointer

    @property
    def val(self):
        return self._val

    @val.setter
    def val(self, val):
        self._val = val

    @property
    def pointer(self):
        return self._pointer

    @pointer.setter
    def pointer(self, pointer):
        self._pointer = pointer

class CustomList:

    def __init__(self, vals):

        if not vals:
            raise ValueError

        self._head = Node(vals[0])
        self._tail = self._head
        self._len = 1
        self._idx = 0

        for i in range(1, len(vals)):
            self.append(vals[i])

        self._curr = self._head

    def __str__(self):

        res = []

        node = self._head

        for _ in range(self._len):
            res += str(node.val)
            node = node.pointer

        return " ".join(res)

    def __getitem__(self, key):

        idx = 0

        res = self._head

        if not res:
            return None

        if 0 <= key < self._len:

            while idx != key:
                res = res.pointer
                idx += 1

        return res.val

    def __setitem__(self, key, value):

        idx = 0

        res = self._head

        if not res:
            return None

        if 0 <= key < self._len:

            while idx != key:
                res = res.pointer
                idx += 1

        res.val = value

    def __iter__(self):
        self._idx = 0
        self._curr = self._head
        return self

    def __next__(self):
        if self._idx < self._len:
            node = self._curr
            self._curr = self._curr.pointer
            self._idx += 1
            return node.val
        else:
            raise StopIteration

    def append(self, val):

        self._tail.pointer = Node(val=val)
        self._tail = self._tail.pointer

        self._len += 1

    def pop(self, idx=-1):

        res = self._head

        if not res:
            return None

        idx = self._len - 1 if idx < 0 else idx

        i = 0
        if idx == i:
            res = self._head
            self._head = self._head.pointer

        if self._len > 1:
            while i + 1 != idx:
                res = res.pointer
                i += 1

            node = res.pointer
            res.pointer = res.pointer.pointer

            res = node

        self._len -= 1
        return res


def tests():

    l = CustomList([1, 2, 3])

    for i in l:
        print(i)

    for i in l:
        print(i)

    for i in l:
        print(i)

    print(l)
    print(l.pop().val)
    print(l)
    print(l.pop().val)
    print(l)
    print(l.pop().val)
    print(l)

    l = CustomList([1, 2, 3])

    print(l)
    print(l.pop(1).val)
    print(l)

    l[1] = 2
    print(l)

def main():

    tests()

if __name__ == "__main__":
    main()