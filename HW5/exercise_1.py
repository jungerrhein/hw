def combine_dicts(*args):
    if args:
        res = args[0]

        for i in range(1, len(args)):
            for k, v in args[i].items():
                if k in res:
                    res[k] += v
                else:
                    res[k] = v

    return res

def main():

    d1 = {"a": 100, "b": 200}
    d2 = {"a": 200, "c": 300}
    d3 = {"a": 300, "d": 100}

    res = combine_dicts(
        d1, d2, d3
    )

    print(res)

if __name__ == "__main__":
    main()