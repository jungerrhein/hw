import numpy as np


def no_numpy_mult(first, second):
    """
    param first: list of "size" lists, each contains "size" floats
    param first: list of "size" lists, each contains "size" floats
    """

    #YOUR CODE: please do not use numpy

    l = len(first)
    result = [[] for _ in range(l)]

    
    for k in range(l):
        for i in range(l):
            row = []
            for j in range(l):
                row += [first[k][j] * second[j][i]]
            
            result[k] += [sum(row)]
    
    return result


def numpy_mult(first, second):
    """
    param first: np.array[size, size]
    param second: np.array[size, size]
    """

    #YOUR CODE: please use numpy

    result = np.dot(first, second)#YOUR CODE: create np.array
    return result

def transform(X, a=1):
    """
    param X: np.array[batch_size, n]
    """

    X_c = np.copy(X)
    
    X_c[:,::2] = X_c[:,::2] ** 3
    X_c[:,1::2] = a
    X_c = X_c[:,::-1]

    #print(X_c)
    #print(X)

    
    #YOUR CODE
    return np.c_[X,X_c]#YOUR CODE


def main():

    arr = np.array(
                [
                    [1,2,3],
                    [4,5,6],
                    [7,8,9]
                ]
            )

    # print(no_numpy_mult(arr, arr))
    #print(numpy_mult(arr, arr))

    print(transform(np.array([[100,200,300,400,500]])))

if __name__ == "__main__":
    main()
