from typing import Union

# new type for type checking
FLOAT_INT = Union[int, float]


def area_triangle(
        a: FLOAT_INT = 4.5,
        b: FLOAT_INT = 5.9,
        c: FLOAT_INT = 9,
        ) -> None:

    """
        Calculate area of triangle by given 3 sides.
        We'll use Heron's formula:

        Area = $\sqrt{p(p-a)(p-b)(p-c)}$, where $p = \frac{a+b+c}{2}$


        a: FLOAT_INT
            first side
        b: FLOAT_INT
            second side
        c: FLOAT_INT
            third side

        return: None
    """

    assert a > 0, "Invalid param a! Need value a > 0"
    assert b > 0, "Invalid param b! Need value b > 0"
    assert c > 0, "Invalid param c! Need value c > 0"

    p = (a + b + c) / 2
    area = (p * (p - a) * (p - b) * (p - c)) ** 0.5

    print(f"Area of triangle: {area:.2f}")


def tests():

    area_triangle(18, 24, 30)  # 216.00
    area_triangle(3, 4, 5)  # 6

    area_triangle(-1, 4, 5)  # assert Error on param `a`


def main():

    area_triangle()


if __name__ == "__main__":
    main()
