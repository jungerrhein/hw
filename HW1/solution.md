# HW1

Calculate area of triangle by given 3 sides.
We'll use Heron's formula:

Area = $\sqrt{p(p-a)(p-b)(p-c)}$, where $p = \frac{a+b+c}{2}$

