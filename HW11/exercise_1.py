"""
Look through filemodules/legb.py.
1) Find a way to callinner_functionwithout moving it from inside ofenclosed_function.
2.1) Modify ONE LINE ininner_function to make it print variable 'a' from global scope.
2.2) Modify ONE LINE ininner_function to make it print variable 'a' form enclosing function.
"""

a = "I am global variable!"


def enclosing_funcion_1():
    a = "I am variable from enclosed function!"

    def inner_function():

        a = "I am local variable!"

        print(a)

    return inner_function

def enclosing_funcion_21():
    a = "I am variable from enclosed function!"

    def inner_function():
        global a

        print(a)

    inner_function()

def enclosing_funcion_22():
    a = "I am variable from enclosed function!"

    def inner_function():

        nonlocal a

        print(a)

    inner_function()

def main():

    enclosing_funcion_1()()
    enclosing_funcion_21()
    enclosing_funcion_22()


if __name__ == "__main__":
    main()