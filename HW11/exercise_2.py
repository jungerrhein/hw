"""
Implement a decoratorremember_resultwhich remembers last result
of function it decorates and prints it before next call.

@remember_result
def sum_list(*args):
    result = ""
    for item in args:
        result += str(item)

    print(f"Current result = '{result}'")
    return result
"""

def remember_result(func):

    res = [None]

    def wrapped_f(*args):

        nonlocal res
        arg = res
        res = args
        return func(*arg)

    return wrapped_f

@remember_result
def sum_list(*args):
    result = ""
    for item in args:
        result += str(item)

    print(f"Current result = '{result}'")
    return result


def main():
    sum_list("a", "b")
    sum_list("abc", "cde")
    sum_list(3, 4, 5)

if __name__ == "__main__":
    main()