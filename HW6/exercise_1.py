class Rectange:

    def __init__(self,
                 a: float,
                 b: float = 5,
                 ):

        assert isinstance(a, float)
        assert isinstance(b, float)

        assert a > 0
        assert b > 0

        self._a = a
        self._b = b

    def GetSideA(self):
        return self._a

    def GetSideB(self):
        return self._b

    def Area(self):
        return self._a * self._b

    def IsSquare(self):
        return self._a == self._b

    def RepalaceSlides(self):
        self._a, self._b = self._b, self._a

    def Perimeter(self):
        return 2 * self._a + 2 * self._b

class ArrayRectangles:

    def __init__(self, n=5):

        assert n > 0

        self._n = n
        self.rectangle_array = [None for _ in range(n)]

    def AddRectangle(self, rec):

        flag = False

        for i in range(self._n):
            if self.rectangle_array[i] == None:
                flag = True
                self.rectangle_array[i] = rec
                break

        return flag

    def NumberMaxArea(self):

        res = [i.Area() for i in self.rectangle_array if i]
        if res:
            return max(res)
        else:
            return 0
    def NumberMinPerimeter(self):

        res = [i.Perimeter() for i in self.rectangle_array if i]
        if res:
            return max(res)
        else:
            return float("inf")

    def NumberSquare(self):

        res = [i.IsSquare() for i in self.rectangle_array if i]

        return len(res)


def main():

    c = (5., 5.)

    r = Rectange(12., 5.)

    ar = ArrayRectangles()

    for i in range(3):
        rec = Rectange(c[0] + i, c[1] + i)
        print(rec._a, rec._b)
        ar.AddRectangle(rec)

    print(ar.NumberMaxArea())
    print(ar.NumberMinPerimeter())
    print(ar.NumberSquare())

if __name__ == "__main__":
    main()