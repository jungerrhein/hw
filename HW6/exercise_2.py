class Employee:

    def __init__(self,
                 name: str,
                 salary: float,
                 ):

        self._name = name
        self._salary = salary
        self._bonus = 0

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, string):
        self._name = string

    @property
    def salary(self):
        return self._salary

    @salary.setter
    def salary(self, val):
        self._salary = val

    def SetBonus(self, val):
        self._bonus = val

    def ToPay(self):
        return self._bonus + self._salary

class SalesPerson(Employee):

    def __init__(self,
                 name: str,
                 salary: float,
                 percent: float
                 ):

        super().__init__(name, salary)
        self._percent = percent
        
    def SetBonus(self, val):

        if 100 <= self._percent < 200:
            self._bonus = val * 2
        elif self._percent >= 200:
            self._bonus = val * 3
        else:
            self._bonus = val

class Manager(Employee):

    def __init__(self,
                 name: str,
                 salary: float,
                 clientAmount: float
                 ):

        super().__init__(name, salary)
        self._quantity = clientAmount

    def SetBonus(self, val):

        if 100 <= self._quantity < 150:
            self._bonus = val + 500
        elif self._quantity >= 150:
            self._bonus = val + 1000
        else:
            self._bonus = val

class Company:

    def __init__(self, arr_emp):

        self._arr_emp = arr_emp

    def GiveEverybodyBonus(self, companyBonus):

        for em in self._arr_emp:
            em.SetBonus(companyBonus)

    def TotalToPay(self):

        res = [em.ToPay() for em in self._arr_emp]
        if res:
            return sum(res)
        else:
            return 0
        
    def NameMaxSalary(self):

        sal = 0
        res = ""

        for em in self._arr_emp:
            val = em.ToPay()
            if val > sal:
                sal = val
                res = em.name

        return res


def main():

    sp1 = SalesPerson("Kek", 42, 99)
    sp1.SetBonus(100)

    sp2 = SalesPerson("Lil", 42, 200)
    sp2.SetBonus(99)
    print(sp2.ToPay())


    sp3 = Manager("Checburek", 300, 200)
    sp3.SetBonus(99)
    print(sp3.ToPay())

    c = Company([sp1, sp2, sp3])
    c.GiveEverybodyBonus(100)
    print(c.TotalToPay())
    print(c.NameMaxSalary())

if __name__ == "__main__":
    main()