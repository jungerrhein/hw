def fizzbuzz(num:int) -> None:

    """
    Fizz buzz
    For multiply of three- print Fizz
    For multiply of five- print Buzz
    For multiply of three and five- print FizzBuzz
    In other cases- number
    """

    assert isinstance(num, int), "Wrong type! Need int"

    if num <= 0 or num > 100:
        print(f"{num} is out of range [1, 100]")
        return

    if not num % 3 and not num % 5:
        print("FizzBuzz")
    elif not num % 3:
        print("Fizz")
    elif not num % 5:
        print("Buzz")
    else:
        print(num)

def tests():

    """
        Base tests for program
    """


    # Basic test
    res = fizzbuzz(63)
    print(res) # Fizz

    res = fizzbuzz(25)
    print(res) # Buzz

    res = fizzbuzz(89)
    print(res) # 89

    res = fizzbuzz(45)
    print(res) # FizzBuzz
    
    res = fizzbuzz(999)
    print(res) # 999 is out of range [1, 100]

def main():

    """
        Main entry point
    """

    tests()

if __name__ == "__main__":
    main()
