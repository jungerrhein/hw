def validate_points(x: float, y: float) -> bool:

    """
    Write a program, that determines wheter the Point A(x, y) is
    in shaded area or not.
    """

    return y >= abs(x) and y <= 1 and y >= 0

def tests():

    """
        Base tests for program
    """


    # Basic test
    count_neg = validate_points(0.5, 0.7)
    assert count_neg == True

    count_neg = validate_points(0.5, 0.3)
    assert count_neg == False

    count_neg = validate_points(1.0, 1.0)
    assert count_neg == True

    count_neg = validate_points(0.0, 0.0)
    assert count_neg == True

def main():

    """
        Main entry point
    """

    tests()

if __name__ == "__main__":
    main()
