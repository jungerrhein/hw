def baccard() -> None:

    """
    simulate the one roud of game baccard
    Rules:
        - cards have a point value:
            * 2-9 catds in each suit are worth face value(in points)
            * 10, jack, king- 0 ponts
            * ace- 1 point
        - Sum pf va;ues cards. If total is more 9 reduce 10 from result:
            (5 + 9 = 14, 14 - 10 = 4 is the result)
        - Player not allowed play joker
    """

    map_cards = {str(i): i % 10 for i in range(1, 11)}
    map_cards["j"] = 0
    map_cards["k"] = 0
    map_cards["a"] = 1

    c1 = input("Play first card: ").strip().lower()
    c2 = input("Play second card: ").strip().lower()

    if (c1 not in map_cards) or (c2 not in map_cards):
        print("Your result: Do not cheat!")
        return

    res = ((map_cards[c1] + map_cards[c2]) % 10)

    print(f"Your result: {res}")


def main():

    """
        Main entry point
    """

    baccard()

if __name__ == "__main__":
    main()
