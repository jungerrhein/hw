"""
Write a function that check whether a string is a palindrome or not.
To check your implementation you can use strings
from here (https://en.wikipedia.org/wiki/Palindrome#Famous_palindromes)
"""

def is_polyndrome(string: str):
    return string[::-1] == string

def main():

    s = "121"
    print(is_polyndrome(s))

    s = "123"
    print(is_polyndrome(s))


if __name__ == "__main__":
    main()