"""
Implement a bunch of functions which receive a changeable number of strings
and return next parameters:
1) characters that appear in all strings
2) characters that appear in at least one string
3) characters that appear at least in two strings
4) characters of alphabet, that were not used in any string
Note: use `string.ascii_lowercase` for list of alphabet letters
"""

import string


def solution(*args):

    concat_s = "".join(args)

    l = [i.lower() for i in args]
    set1 = set()
    set2 = set(string.ascii_lowercase)
    set4 = set(string.ascii_lowercase)

    set3 = []

    for i in l:
        set1 |= set(i)
        set2 &= set(i)

    for i in set4:
        if concat_s.count(i) >= 2:
            set3 += [i]

    set4 ^= set1

    set1 = sorted("".join([i for i in set1]))
    set2 = sorted("".join([i for i in set2]))
    set3 = sorted("".join(set3))
    set4 = sorted("".join([i for i in set4]))


    print(set1, set2, set3, set4)

def main():

    # l = ["qwe", "asdxzcqwe", "qweasd"]
    # solution(*l)

    l =  ["hello", "world", "python", ]
    solution(*l)

if __name__ == "__main__":
    main()