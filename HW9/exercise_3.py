"""Implement a function `get_shortest_word(s: str) -> str`
which returns the shortest word in the given string.
The word can contain any symbols except whitespaces
(` `, `\n`, `\t` and so on).
If there are multiple shortest words in the string with a same length return the
word that occurs first.
Usage of any split functions is forbidden.
Example:
    get_shortest_word('Python is simple and effective!')
    'is'
    get_shortest_word('Any pythonistalike namespaces a lot, a? O')
    'a'
"""

def get_shortest_word(string: str):

    ans = ""
    len_ans = float("inf")

    i = 0
    while i < len(string):
        counter = 0
        while i < len(string) and  not string[i].isspace():
            i += 1
            counter += 1

        if counter < len_ans:
            len_ans = counter
            ans = string[i - counter:i]

        i += 1

    return ans


def main():

    s = "Python is simple and effective!"
    print(get_shortest_word(s))

    s = "Any pythonistalike namespaces a lot, a? O"
    print(get_shortest_word(s))

    s = ""
    print(get_shortest_word(s))

if __name__ == "__main__":
    main()