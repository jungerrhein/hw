"""
Implement a function which receives a string and replaces
all `"` symbols with `'` and vise versa.
"""

def replace_func(string: str, to: str, by: str):

    return "".join([i if i != to else by for i in string])

def main():

    s = '"qwe"'

    to = "\""
    by = "\'"
    print(replace_func(s, to, by))

if __name__ == "__main__":
    main()