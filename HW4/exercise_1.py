from enum import Enum


class SortOrder(Enum):
    ASCENDING = 0
    DESCENDING = 1

def isSorted(array: list, order: SortOrder):

    if order == SortOrder.ASCENDING:
        res = array == sorted(array)
    else:
        res = array == sorted(array, reverse=True)

    return res

def main():

    l = [3, 2, 1]
    print(isSorted(l, SortOrder.DESCENDING))

if __name__ == "__main__":
    main()