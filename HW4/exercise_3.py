def MultArithmeticElements(
                            a: int=5,
                            t: int=3,
                            n: int=4,
                            ):
    res = a

    for i in range(1, n):
        res *= (a + (t * i))

    return res

def main():
    print(MultArithmeticElements())

if __name__ == "__main__":
    main()