def SumGeometricElements(
                            a: int = 100,
                            t: float = 0.5,
                            alim: int = 20,
                    ):

    res = a

    while a * t > alim:
        a *= t
        res += a

    return res

def main():

    print(SumGeometricElements())

if __name__ == "__main__":
    main()