from enum import Enum


class SortOrder(Enum):
    ASCENDING = 0
    DESCENDING = 1

def isSorted(array: list, order: SortOrder):

    if order == SortOrder.ASCENDING:
        res = array == sorted(array)
    else:
        res = array == sorted(array, reverse=True)

    return res

def transform(array: list, order):

    if isSorted(array, order):
        for i in range(len(array)):
            array[i] += i

    return array


def main():
    pass

if __name__ == "__main__":
    main()