Solution from Ishchenko Roman

[[_TOC_]]

## Homework 1
Calculate area of triangle by given lenght of 3 sides
[a relative link](./HW1/solution.py)

## Homework 2
* Count negative values in list [a relative link](./HW2/exercise_1.py )
* Change positions [a relative link](HW2/exercise_2.py )
* Swap 'reasonable' on 'unreasonable' [a relative link](HW2/exercise_3.py )

## Homework 3
* Is Point A(x, y) in shaded area or not.[a realtive link](HW3/exercise_1.py )
* Fizz buzz [a realtive link](HW3/exercise_2.py )
* Simulate the one roud of game baccard [a relative link](HW3/exercise_3.py )

## Homework 4
* `isSorted` [a realtive link](HW4/exercise_1.py )
    * Check if array sorted in some way (ascending or descending).
    Used lazy mode if equal to sorted array. 
* `transform` [a realtive link](HW4/exercise_2.py )
    * Used function from previous exercise if is sorted
    just add index with respect to position of element.
* `MultArithmeticElements` [a relative link](HW4/exercise_3.py )
    * Just implement logic (modification arifmetic progression)
* `SumGeometricElements` [a relative link](HW4/exercise_4.py )
    * Just implement logic (modification geometric progression)

## Homework 5
* `combine_dicts` [a realtive link](HW5/exercise_1.py )
    * Merge dicts- just iter and add elements.
* `class CustomList` [a realtive link](HW5/exercise_2.py )
    * Implementation of raw single linkage list
    
## Homework 6
* `class Rectange [+ bonus class ArrayRectangles]` [a realtive link](HW6/exercise_1.py )
    * Implement classes w.r.t. technical specification.
* `class Employee, class SalesPerson, class Manager, [ +bonus class Company]` [a realtive link](HW6/exercise_2.py )
    * Implement classes w.r.t. technical specification.

## Homework 7
* `foo` [a realtive link](HW7/exercise_1.py )
    *     foo() # 8 None
          foo() # 1 AssertionError: Not enough

* `foo` [a realtive link](HW7/exercise_2.py )
    *   ```
        After bar
        NameError: name 'bar' is not defined
        ```
* `baz` [a realtive link](HW7/exercise_3.py )
    * 
      ```
        returned value 2
      ```
* `foo` [a realtive link](HW7/exercise_4.py )
    * 
      ```
         else:
         ^
         SyntaxError: invalid syntax
      ```
*  [a realtive link](HW7/exercise_5.py )
    * 
      ```
            ./hw/HW7/exercise_5.py
            Traceback (most recent call last):
              File "./hw/HW7/exercise_5.py", line 3, in <module>
                raise "Error"
            TypeError: exceptions must derive from BaseException
            
            During handling of the above exception, another exception occurred:
            
            Traceback (most recent call last):
              ./hw/HW7/exercise_5.py", line 6, in <module>
                except "Error":
            TypeError: catching classes that do not inherit from BaseException is not allowed
            
            Process finished with exit code 1
      ```
      
*  [a realtive link](HW7/exercise_6.py )
    * 
      ```
            after 9223372036854775807 times 'Input not found' raised OSError
      ```
      
## Homework 8
    * `Fog of war (can't find exercises)`

## Homework 9
* `replace_func` [a realtive link](HW9/exercise_1.py )
    * Implement a function which receives a string and replaces
            all " symbols with ' and vise versa.
* `is_polyndrome` [a realtive link](HW9/exercise_2.py )
    * Write a function that check whether a string is a palindrome or not.
To check your implementation you can use strings
from here (https://en.wikipedia.org/wiki/Palindrome#Famous_palindromes)
* `get_shortest_word` [a realtive link](HW9/exercise_3.py )
    * Implement a function `get_shortest_word(s: str) -> str`
which returns the shortest word in the given string.
The word can contain any symbols except whitespaces
(` `, `\n`, `\t` and so on).
If there are multiple shortest words in the string with a same length return the
word that occurs first.
Usage of any split functions is forbidden.
Example:
    get_shortest_word('Python is simple and effective!')
    'is'
    get_shortest_word('Any pythonistalike namespaces a lot, a? O')
    'a'.
    Use move pointer of string, ~ O(n)
* `solution` [a realtive link](HW9/exercise_4.py )
    * Implement a bunch of functions which receive a changeable number of strings
and return next parameters:
        1) characters that appear in all strings
        2) characters that appear in at least one string
        3) characters that appear at least in two strings
        4) characters of alphabet, that were not used in any string
        Note: use `string.ascii_lowercase` for list of alphabet letters
        
## Homework 10
* `replace_func` [a realtive link](HW10/exercise_1.py )
    * Open file `data/unsorted_names.txt` in data folder.
Sort the names and write them to a new file called `sorted_names.txt`.
Each name should start with a new line as in the following.
    Read, sort, write
* `most_common_words` [a realtive link](HW10/exercise_2.py )
    * standardize word, count using `numpy.unique`
* `most_common_words` [a realtive link](HW10/exercise_3.py )
    * 1)File `data/students.csv` stores information about students in
      2_CSV(https://en.wikipedia.org/wiki/Comma-separated_values) format.
This file contains the student’s names, age and average mark.
      3) Implement a function which receives file path and returns names of top performer students
      4) Implement a function which receives the file path with students info and writes CSV student information to the new file in descending order of age.
    
    `pandas.DataFrame.read_csv + pandas.DataFrame.sort_values + pandas.DataFrame.to_csv`  
    Any restriction didn't found.
    
## Homework 11 (one of the most interesting tasks)
* `inner_function` [a realtive link](HW11/exercise_1.py )
    * Look through file `modules/legb.py`.  
    1.) Find a way to call inner_function without moving it from inside of enclosed_function.  
    2.1) Modify ONE LINE ininner_function to make it print variable 'a' from global scope.  
    2.2) Modify ONE LINE ininner_function to make it print variable 'a' form enclosing function.
    
    Use `local/ nonlocal/ global keywords`
* `remember_result` [a realtive link](HW11/exercise_2.py )
    * Implement a decorator remember_result which remembers last result
of function it decorates and prints it before next call.
```
    @remember_result
    def sum_list(*args):
        result = ""
        for item in args:
            result += str(item)
    
        print(f"Current result = '{result}'")
        return result
```

    Again use `nonlocal` keyword, inner function can get values from scope above.
    This has names "clousure".
    
* `remember_result` [a realtive link](HW11/exercise_3.py )
    * Implement a decorator call_once which runs a function or method once
and caches the result. All consecutive calls to this function should
return cached result no matter the arguments.
    ```
        @call_once
        def sum_of_numbers(a, b):
            return a + b
        
        print(sum_of_numbers(13, 42))
        >> 55
        print(sum_of_numbers(999, 100))
        >> 55
        print(sum_of_numbers(134, 412))
        >> 55
        print(sum_of_numbers(856, 232))
        >> 55
    ```
  
    Similar to exercise above

## Homework 12 (one of the most interesting tasks)
* `text` [a realtive link](HW12/exercise_1.txt )
* `text` [a realtive link](HW12/exercise_2.txt )