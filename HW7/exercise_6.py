flag = False
while not flag:
    try:
        filename = input("Enter filename: ")
        filename = open(filename, "r")
    except:
        print("Input not found")

"""
after 9223372036854775807 times 'Input not found' raised OSError
"""