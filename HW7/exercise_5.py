try:
    if "1" != 1:
        raise "Error"
    else:
        print("Error has not occurred")
except "Error":
    print("Error has occurred")

"""
./hw/HW7/exercise_5.py
Traceback (most recent call last):
  File "./hw/HW7/exercise_5.py", line 3, in <module>
    raise "Error"
TypeError: exceptions must derive from BaseException

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  ./hw/HW7/exercise_5.py", line 6, in <module>
    except "Error":
TypeError: catching classes that do not inherit from BaseException is not allowed

Process finished with exit code 1
"""