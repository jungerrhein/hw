def baz():

    try:
        return 1
        with open("/tmp/logs.txt") as file:
            print(file.read())
            return
    finally:
        return 2

def main():
    # 2
    res = baz()
    print(res)

if __name__ == "__main__":
    main()