def foo():

    try:
        bar(x, 4)
    finally:
        print("After bar")
    print("Or this after bar")

def main():

    """
        # After bar
        # NameError: name 'bar' is not defined
    """

    foo()

if __name__ == "__main__":
    main()