def foo():
    a = int(input("Enter number: "))
    b = 7
    assert a > b, "Not enough"

def main():

    foo() # 8 None
    foo() # 1 AssertionError: Not enough

if __name__ == "__main__":
    main()