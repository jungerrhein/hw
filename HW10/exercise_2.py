"""
Implement a function which search for most common words in the file.
Use `data/lorem_ipsum.txt` file as an example.

Note: Remember about dots, commas, capital letters etc.
"""

import numpy as np
import re


def most_common_words(filepath, number_of_words=3):

    with open(filepath, "r") as f:

        lines = f.read()

    lines = [re.sub(r'\W+', '', i.lower()) for i in lines.split()]
    letters, count = np.unique(lines, return_counts=True)
    print(letters)
    print(count)
    idx = np.argsort(count)[:number_of_words]

    return letters[idx]

def main():

    filepath = "data/lorem_ipsum.txt"
    print(most_common_words(filepath))

if __name__ == "__main__":
    main()