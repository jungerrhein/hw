"""
Open file `data/unsorted_names.txt` in data folder.
Sort the names and write them to a new file called `sorted_names.txt`.
Each name should start with a new line as in the following
"""

def solution():

    with open("data/unsorted_names.txt", "r") as f:

        l = sorted(f.read().split())

    with open("./sorted_names.txt", "w") as f:
        f.write("\n".join(l))

def main():
    solution()

if __name__ == "__main__":
    main()