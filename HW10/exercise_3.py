"""
File `data/students.csv` stores information about students in
CSV(https://en.wikipedia.org/wiki/Comma-separated_values) format.
This file contains the student’s names, age and average mark.
1) Implement a function which receives file path and returns names of top performer students
2) Implement a function which receives the file path with students info and writes CSV student information to the new file in descending order of age.
"""

import pandas as pd

def get_top_performers(file_path, number_of_top_students=5):

    df = pd.read_csv(file_path)

    return df.sort_values(by="average mark", ascending=False).iloc[:number_of_top_students]["student name"].values

def descending_age(file_path):

    df = pd.read_csv(file_path)

    df = df.sort_values(by="average mark", ascending=False)
    df.to_csv("./age_desc.csv", index=False)

def main():

    path = "./data/students.csv"
    print(get_top_performers(path))
    descending_age(path)


if __name__ == "__main__":
    main()